﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Soln_ListItemTask.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
</head>
<body>
    <form id="form1" runat="server">
    <div class="w3-container w3-responsive "  >
    
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table class="w3-table-all" style="margin-left:auto; margin-right:auto; width:300px">
                    <tr>
                        <td>
                            <asp:ListBox id="listbox1" runat="server" >
                                <asp:ListItem>India</asp:ListItem>
                                <asp:ListItem>Pakistan</asp:ListItem>
                                <asp:ListItem>England</asp:ListItem>
                                <asp:ListItem>Spain</asp:ListItem>
                                <asp:ListItem>Italy</asp:ListItem>
                                <asp:ListItem>France</asp:ListItem>
                                <asp:ListItem>Germany</asp:ListItem>
                                <asp:ListItem>Russia</asp:ListItem>
                            </asp:ListBox>
                            <td />
                            <asp:Button ID="btnlb1singleremove" runat="server" Text="--->" OnClick="btnlb1singleremove_Click"  />
                            <asp:Button ID="btnlb2singleremove" runat="server" Text="<---" OnClick="btnlb2singleremove_Click"  />
                            <asp:Button ID="btnlb1removeall" runat="server" Text=" >> " OnClick="btnlb1removeall_Click" />
                            <asp:Button ID="btnlb2removeall" runat="server" Text=" << " OnClick="btnlb2removeall_Click"  />
                            <br />
                            <td>                            
                                <asp:ListBox ID="listbox2" runat="server" />
                                </td>                            
                            </tr>             
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
