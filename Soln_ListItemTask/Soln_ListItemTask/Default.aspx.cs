﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Soln_ListItemTask
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (IsPostBack==false)
            //{
            //    btnlb1singleremove.Enabled = false;
            //    btnlb2singleremove.Enabled = false;
            //    btnlb1removeall.Enabled = false;
            //    btnlb2removeall.Enabled = false;
            //}
        }

        protected void btnlb1singleremove_Click(object sender, EventArgs e)
        {
            //if (listbox1.SelectedItem != null)
            //{
            //    listbox2.Items.Add(listbox1.SelectedItem);
            //    listbox1.Items.Remove(listbox1.SelectedItem);
            //}
            listbox2.Items.Add(listbox1.SelectedItem);
            listbox1.Items.RemoveAt(listbox1.SelectedIndex);

        }

        protected void btnlb2singleremove_Click(object sender, EventArgs e)
        {
            //if (listbox2.SelectedItem != null)
            //{
            //    listbox1.Items.Add(listbox2.SelectedItem);
            //    listbox2.Items.Remove(listbox2.SelectedItem);
            //}
            listbox1.Items.Add(listbox2.SelectedItem);
            listbox2.Items.RemoveAt(listbox2.SelectedIndex);
        }

        protected void btnlb1removeall_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < listbox1.Items.Count; i++)
            {
                listbox2.Items.Add(listbox1.Items[i].ToString());
            }
            listbox1.Items.Clear();
        }

        protected void btnlb2removeall_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < listbox2.Items.Count; i++)
            {
                listbox1.Items.Add(listbox2.Items[i].ToString());
            }
            listbox2.Items.Clear();
        }

      
       
    }
}